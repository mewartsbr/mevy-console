  
/**
 * @name Moment.js
 * @description Pacote para traduzir timestamp
 */
const moment = require("moment");
/**
 * @name colors
 * @description Para colorir console
 */
const colors = require("colors");
const timestampdisplay = `[TIMESTAMP: ${moment(Date.now()).format()}]`.black
const timestampdisplayDebug = `[TIMESTAMP: ${moment(Date.now()).format()}]`.white
const logs = "[LOG]".black
const warnls = "[WARN]".black
const errorls = "[ERROR]".white
const debugls = "[DEBUG]".black


/**
 * @name log
 * @color Blue
 */
exports.log = (log) => console.log(`${timestampdisplay.bgBlue}${logs.bgBlue}`, ` ${log}`)

/**
 * @name Warn
 * @color Yellow
 */
exports.warn = (warn) => console.warn(`${timestampdisplay.bgYellow}${warnls.bgYellow}` + ` ${warn}`)



/**
 * @name Error
 * @color Red
 */
exports.error = (error) =>   console.error(`${timestampdisplay.bgRed}${errorls.bgRed}` + ` `+ ` ${error.white}`.bgRed)

/**
 * @name Error
 * @color Magenta
 * @description DescriÃ§Ã£o do erro vai mostrar cor 
 */
exports.bug = (bug) => console.debug(`${timestampdisplayDebug.bgMagenta}${debugls.bgMagenta}` + ` ${bug}`)

