

# Console Mevy - NODEJS


Meu primeiro pacote para NodeJS 

 - Correções de bug
    - [x] Bug no timestamp
    - [x] Esqueci de adicionar colors no package.json

 - Pacotes

   - [x] [Colors](https://github.com/Marak/colors.js)
   - [x] [Moment.js](https://github.com/moment/moment)
          - Website: http://momentjs.com/


 # Exemplo
   ![Exemplo](https://xyz.meby.xyz/file/nakaapi/3NtYkgSUH7Q.png)

 ```js
 const MevyLogs = require("mevy-console")

 MevyLogs.log("teste")

 MevyLogs.error("teste")

 MevyLogs.warn("teste")

 MevyLogs.bug("teste")
 ```
 